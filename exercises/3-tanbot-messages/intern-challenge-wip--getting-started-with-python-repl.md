# intern-challenge-14 Getting Started with the iPython REPL

If you've never used the `ipython` "REPL" or haven't heard of it, you might want to try.
It's my preferred workflow for building new python modules and packages.
It's especially useful when you're working with new data or python packages you haven't used in a while.
Here's a good introduction: [Getting started with **i**python](https://sodocumentation.net/ipython)

## Resources

- [Getting started with **i**python](https://sodocumentation.net/ipython)
- [ipython docs](https://ipython.org/documentation.html) 
