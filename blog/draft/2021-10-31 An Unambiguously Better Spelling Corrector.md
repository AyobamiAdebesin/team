# An Unambiguously Better Spelling Corrector

## Peter Norvig - the King of spelling correction

[Peter Norvig's one-page explanation](ttps://norvig.com/spell-correct.html) (with code) of how to do spelling is a masterpiece of smart thinking.
It has stood the test of time.
I haven't found a more elegant approach yet.

So Peter's code is the basis for this improved approach to spelling correction.
We just show you how to find the datasets you need (dictionaries) to do it for just about any language.
And once you've mastered that, we'll show you how to use neighboring words and machine learning to resolve ambiguities.

## Ambiguity

Ambiguity is when there are multiple interpretations of something and it's not clear what the correct course of action is. 
For a spelling corrector, even a with all the time and resources in the world can't decide the correct correction for these misspelling:

"Weeat spelt wrong."

It could be:

"Wheat spelled wrong."

or 

"We eat spelt wrong."

If this one seems to obscure, consider Norvig's examples. 
If you saw the word "lates", would your algorithm add a "t" in the middle to make lattes, or at the end to make "latest".

But if the word occurred in a sentence like "I'll have some lates," you'd only need to know about the neighboring word "some" for your algorithm to make the correct correction.
But your algorithm might still have trouble with:

"Give me the lates."

Someone could be asking for the latest gossip or news.
Or they might want more than one latte.
Because Wikipedia titles often consist of a single word.
And even a properly spelled word can mean many different things.
So Wikipedia contributors have created 10s of thousands of [disambiguation pages](https://en.wikipedia.org/wiki/Wikipedia:Disambiguation#Disambiguation_pages) to help you find what you're really looking for.

But can you think of a way to resolve this "dispute" within your spelling corrector algorithm?

## Spanish

Extending a spelling corrector to other languages is not straightforward, even for languages without complicated spelling and grammar rules like Chinese and Japanese.
But you can go a long way and achieve a 90% solution by just expanding your training corpus (collection of documents) to include words from your target language.
For Spanish, your best bet may be the [Spanish version of Wikipedia](https://es.wikipedia.org/).

And you don't have to parse all that HTML.
You can download thousands of pages of spanish text and structured data using the API.
And there's a Python package for that...

```bash
$ pip install wikipedia
```

You just need to switch it to Spanish (Espanol):

```
>>> import wikipedia as wiki
>>> wiki.set_lang("es")
>>> titles = wiki.search('Spanish')
>>> titles
['Spanish',
 'Spanish Fork',
 'Spanish Fly (película)',
 'Spanish Movie',
 'Spanish Valley',
 'The Spanish Recordings: Aragón & València',
 'The Spanish Recordings: Extremadura',
 'The Spanish Recordings: Basque Country: Navarre',
 'Spanish Company Number One',
 'Spanish Town (desambiguación)']
>>> page = wiki.WikipediaPage(titles[1])
>>> page.title
'Spanish Fork'
>>> page.coordinates
(Decimal('40.1044...'),
 Decimal('-111.64...'))
>>> page.content
'Spanish Fork (en español: Tenedor Español) es una ciudad ubicada en el condado de Utah en el estado estadounidense de Utah. En el año 2000 tenía una población de 31.497 habitantes. Spanish Fork se localiza dentro de los límites metropolitanos de las ciudades de Provo/Orem.\n\n\n== Geografía ==\nSpanish Fork se encuentra ubicado en las coordenadas 40°9′46″N 111°36′15″O. Según la Oficina del Censo, la localidad tiene un área total de 29,9 km² (11,5 mi²), de la cual 99,45% es tierra.[2]\u200b\n\n\n== Demografía ==\nSegún la Oficina del Censo en 2000,[3]\u200b habían 31.497 personas y 7.359 familias residentes en el lugar, 90.2% de los cuales eran personas de raza blanca.\nSegún la Oficina del Censo en 2000 los ingresos medios por hogar en la localidad eran de $62,805, y los ingresos medios por familia eran $64,909. La renta per cápita para la localidad era de $17,162. Alrededor del 6.2% de la población de Spanish Fork estaba por debajo del umbral de pobreza.[3]\u200b\n\n\n== Referencias ==\n\n\n== Enlaces externos ==\n Portal:Utah. Contenido relacionado con Utah.'
>>> page.coordinates
(Decimal('40.10444444000000174810338648967444896697998046875'),
 Decimal('-111.6400000000000005684341886080801486968994140625'))


But if you're willing to give up cleverness, elegance and a bit of speed, you can improve
